import {all} from 'redux-saga/effects'
import {watchLogin} from './saga'

export default function* rootSaga(){
    yield all(
        [
            watchLogin(),
        ]
    );
}

