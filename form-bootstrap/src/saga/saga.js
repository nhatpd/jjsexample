import {isLogin, isLogout} from "../constants/ActionTypes"

import {takeEvery} from "redux-saga/effects"

function* login(){
    yield console.log('this is login saga');
}

function* logout(){
    yield console.log('this is logout saga');
}

export function* watchLogin(){
    yield takeEvery(isLogin, login);
    yield takeEvery(isLogout, logout);

}