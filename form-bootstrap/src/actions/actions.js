import * as types from '../constants/ActionTypes';

/*
 * action creators
 */
export function login(username, password){
    return{
        type : types.isLogin,
        username,
        password, 
    }
}

export function logout(){
    return{
        type : types.isLogout,
    }
}