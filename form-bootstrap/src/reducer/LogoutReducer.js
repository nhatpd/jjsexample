import { isLogout } from "../constants/ActionTypes";

const initState = {
  isLogedIn: false,
  username: "",
  password: ""
};

const logOutReducer = (state = initState, action) => {
  if (action.type === isLogout) {
    return {
      isLogedIn: false,
      username: "",
      password: ""
    };
  }
  return state;
};

export default logOutReducer;