import loginReducer from './LoginReducer';
import logOutReducer from './LogoutReducer'
import { combineReducers }from 'redux';

const myReducer = combineReducers(
    {
        loginReducer : loginReducer,
        logOutReducer : logOutReducer,
    }
);

export default myReducer;