import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "../actions/actions";

class LogOut extends React.Component {
  LogOut = e => {
    e.preventDefault();
    this.props.logout();
    this.props.history.push("/");
  };
  render() {
    console.log("aaa", this.props.history.location.state);
    if (this.props.history.location.state) {
    return (
      <div className="container-fluid">
        <form onSubmit={this.LogOut}>
          <h1>Wellcome {this.props.history.location.state.name}</h1>
          <input type="submit" className="btn btn-primary" value="Logout" />
        </form>
      </div>
    );
    } else {
      return(
       <div>
          {this.props.history.push("/login")}
       </div>
      )
    }
  }
}

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
});

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(LogOut)
);
