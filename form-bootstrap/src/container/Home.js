import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";
import { withRouter } from "react-router-dom";

class Home extends React.Component {
    Login = e => {
        e.preventDefault();
        this.props.history.push('/login')
    }
  render() {
    return (
      <div className="container-fluid">
        <h1>Home</h1>
        <div
          className="btn-group btn-group-lg"
          role="group"
          aria-label="Basic example"
        >
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.Login}
          >
            Login
          </button>

        </div>
      </div>
    );
  }
}

export default withRouter(Home) ;
