import React from "react";
import CheckLogin from "./CheckLogin";
import LogOut from "./Logout";
import Home from "./Home";
import { BrowserRouter as Router, Route } from "react-router-dom";

class App extends React.Component {
  routes = [
    {
      path: "/",
      component: Home
    },
    {
      path: "/login",
      component: CheckLogin
    },
    {
      path: "/logout",
      component: LogOut
    }
  ];

  RouteWithNoSub(route) {
    return <Route exact path={route.path} component={route.component} />;
  }

  render() {
    return (
      <Router>
        <div>
          {this.routes.map((route, i) => (
            <this.RouteWithNoSub key={i} {...route} />
          ))}
        </div>
      </Router>
    );
  }
}

export default App;
