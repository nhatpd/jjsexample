import React from "react";
import Home from "./Login";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { login } from "../actions/actions";

let importText = "";

class Login extends React.Component {
  login = (username, password) => {
    this.props.logined(username, password);
  };

  componentDidUpdate() {
    if (this.props.loginReducer.isLogedIn) {
      this.props.history.push("/logout", {
        name: this.props.loginReducer.username,
      });
    }
  }
  render() {
    return (
      <div>
        <Home login={this.login} Check={importText} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loginReducer: state.loginReducer
});

const mapDispatchToProps = dispatch => ({
  logined: (username, password) => dispatch(login(username, password)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
