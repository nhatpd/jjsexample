import React from "react";
import "../App.css";
import "bootstrap/dist/css/bootstrap.css";

const validateInput = (type, checkingText) => {
  if (type === "username") {
    if (checkingText !== null && checkingText !== "") {
      return {
        isInputValid: false,
        errorMessage: ""
      };
    } else {
      return {
        isInputValid: true,
        errorMessage: "Không được để trống"
      };
    }
  }
  if (type === "password") {
    if (checkingText !== null && checkingText !== "") {
      return {
        isInputValid: false,
        errorMessage: ""
      };
    } else {
      return {
        isInputValid: true,
        errorMessage: "Không được để trống"
      };
    }
  }
  return {
    isInputValid: true,
    errorMessage: "Không được để trống"
  };
};

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: {
        value: "",
        isInputValid: true,
        errorMessage: ""
      },
      password: {
        value: "",
        isInputValid: true,
        errorMessage: ""
      }
    };
  }

  handleInput = event => {
    const { name, value } = event.target;
    const newState = { ...this.state[name] }; /* dummy object */
    newState.value = value;
    this.setState({ [name]: newState });
  };

  handleInputValidation = event => {
    const { name } = event.target;
    const { isInputValid, errorMessage } = validateInput(
      name,
      this.state[name].value
    );
    const newState = { ...this.state[name] }; /* dummy object */
    newState.isInputValid = isInputValid;
    newState.errorMessage = errorMessage;
    this.setState({ [name]: newState });
  };

  Login = e => {
    e.preventDefault();
    const inputUsername = this.refs.inputUsername.value;
    const inputPassword = this.refs.inputPassword.value;
    this.props.login(inputUsername, inputPassword);
    this.refs.inputUsername.value = "";
    this.refs.inputPassword.value = "";
    this.refs.inputUsername.focus();
    this.refs.inputPassword.focus();
  };

  render() {
    return (
      <div className="container-fluid">
        <h1>Form Login</h1>
        <form onSubmit={this.Login}>
          <div className="form-group">
            <label>Username</label>
            <input
              name="username"
              type="text"
              className="form-control"
              placeholder="Username"
              ref="inputUsername"
              onChange={this.handleInput}
              onBlur={this.handleInputValidation}
            />
            <small className="text-danger">
              {this.state.username.errorMessage}
            </small>
          </div>

          <div className="form-group">
            <label>Password</label>
            <input
              name="password"
              type="password"
              className="form-control"
              placeholder="Password"
              ref="inputPassword"
              onChange={this.handleInput}
              onBlur={this.handleInputValidation}
            />
            <small className="text-danger">
              {this.state.password.errorMessage}
            </small>
          </div>
          <input
            type="submit"
            className="btn btn-primary"
            value="Login"
            disabled={
              this.state.username.isInputValid ||
              this.state.password.isInputValid
            }
          />
        </form>
      </div>
    );
  }
}

export default Home;
